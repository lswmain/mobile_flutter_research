import 'package:flutter/material.dart';
import 'package:rounded_modal/rounded_modal.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class FirstPage extends StatelessWidget {
  String orangeColor = '#e87722';

  @override
  Widget build(BuildContext context) {

    Container textHeading(String textTitle){
      return Container(
        padding: const EdgeInsets.only(bottom: 20),
        child: Text(textTitle,
        textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.grey[800],
            fontWeight: FontWeight.bold,
          ),
        )
      );
    }

    Container textField(String hintText, IconData suffixicon) {
      return Container(
        padding: const EdgeInsets.only(bottom: 8),
        child: TextField(
          decoration: InputDecoration(
            filled: true,
            fillColor: Colors.white,
            border: OutlineInputBorder(),
            hintText: hintText,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              borderSide: BorderSide(color: Colors.grey[200]),
            ),
            suffixIcon: IconButton(
              onPressed: () {},
              icon: Icon(suffixicon),
            ),
          )
        ),
      );
    }

    Container button_next(String buttonTitle, int step) {
      return Container(
        padding: const EdgeInsets.only(top: 20),
        child: SizedBox(
          // width: 50,
          height: 40,
          child: RaisedButton(
            color: Colors.teal[800],
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(20)
            ),
            onPressed: () => {
              Navigator.of(context).pop(),
              showRoundedModalBottomSheet(
              // animationController:
              context: context,
              radius: 20, // This is the default
              // color: Colors.white, // Also default
              builder: (builder) {
                switch (step) {
                  case 1:
                    return new Container(
                      height: 450,
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(20),
                          topRight: const Radius.circular(20)),
                        image: DecorationImage(
                          image: AssetImage("images/bgOrange.png"), fit: BoxFit.fill)),
                      // color: Colors.transparent, //could change this to Color(0xFF737373), //so you don't have to change MaterialApp canvasColor
                      padding: EdgeInsets.only(top: 20),
                      child: ListView(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        scrollDirection: Axis.vertical,
                        children: <Widget>[
                          textHeading('กรอกรหัส OTP เพื่อยืนยัน'),
                          button_next('ดำเนินการต่อ', 2),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ยกเลิก',
                            ),
                          )
                        ],
                      )
                    );
                    break;

                  case 2:
                    return new Container(
                      height: 450,
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(20),
                          topRight: const Radius.circular(20)),
                        image: DecorationImage(
                          image: AssetImage("images/bgOrange.png"), fit: BoxFit.fill)),
                      // color: Colors.transparent, //could change this to Color(0xFF737373), //so you don't have to change MaterialApp canvasColor
                      padding: EdgeInsets.only(top: 20),
                      child: ListView(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        scrollDirection: Axis.vertical,
                        children: <Widget>[
                          textHeading('เลือกกรมธรรม์ที่ต้องการ'),
                          button_next('ดำเนินการต่อ', 3),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ยกเลิก',
                            ),
                          )
                        ],
                      )
                    );
                    break;

                  case 3:
                    return new Container(
                      height: 450,
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(20),
                          topRight: const Radius.circular(20)),
                        image: DecorationImage(
                          image: AssetImage("images/bgOrange.png"), fit: BoxFit.fill)),
                      // color: Colors.transparent, //could change this to Color(0xFF737373), //so you don't have to change MaterialApp canvasColor
                      padding: EdgeInsets.only(top: 20),
                      child: ListView(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        scrollDirection: Axis.vertical,
                        children: <Widget>[
                          textHeading('ตั้งค่ารหัสความปลอดภัย'),
                          PinCodeTextField(
                            length: 6,
                            obsecureText: false,
                            backgroundColor: new Color(int.parse(orangeColor.substring(1, 7), radix: 16) + 0xFF000000),
                            animationType: AnimationType.fade,
                            shape: PinCodeFieldShape.box,
                            animationDuration: Duration(milliseconds: 100),
                            borderRadius: BorderRadius.circular(5),
                            borderWidth: 1,
                            inactiveColor: Colors.grey,
                            fieldHeight: 50,
                            fieldWidth: 40,
                            onChanged: (value) {
                              print(value);
                            },
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ยกเลิก',
                            ),
                          )
                        ],
                      )
                    );
                    break;

                  default:
                }
              })
            },
            child: Text(buttonTitle,style: TextStyle(color: Colors.white)),
          )
        )
      );
    }

    Container cardSelect(String nameCard, int step){
      return Container(
        // width: 170,
        width: MediaQuery.of(context).size.width * 0.45,
        child: Card(
        // margin: const EdgeInsets.all(20),
          color: Colors.grey[50],
          child: InkWell(
            splashColor: Colors.blue.withAlpha(30),
            onTap: () {
              print('Card tapped.');
              Navigator.of(context).pop();
              // showModalBottomSheet(
              //   shape: RoundedRectangleBorder(
              //     borderRadius: BorderRadius.only(
              //       topLeft: const Radius.circular(10.0),
              //       topRight: const Radius.circular(10.0)
              //     )
              //   ),
              //   context: context,
              //   builder: (BuildContext context) {
              //   // animation: curve,
              //   return Container(
              //     height: 500,
              //     child: Padding(
              //       padding: const EdgeInsets.all(32.0),
              //       child: Text('This is the modal bottom sheet. Slide down to dismiss.',
              //         textAlign: TextAlign.center,
              //         style: TextStyle(
              //           color: Theme.of(context).accentColor,
              //           fontSize: 24.0,
              //         ),
              //       ),
              //     )
              //   );
              // });
              showRoundedModalBottomSheet(
              // animationController:
              context: context,
              radius: 20, // This is the default
              color: Colors.white, // Also default
              builder: (builder) {
                switch (step) {
                  case 0:

                  case 1:
                    return new Container(
                      height: 450,
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(20),
                          topRight: const Radius.circular(20)),
                          image: DecorationImage(
                            image: AssetImage("images/bgOrange.png"), fit: BoxFit.fill)),
                      // color: Colors.transparent, //could change this to Color(0xFF737373), //so you don't have to change MaterialApp canvasColor
                      padding: EdgeInsets.only(top: 20),
                      child: ListView(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        scrollDirection: Axis.vertical,
                        children: <Widget>[
                          textHeading('กรอกข้อมูลผู้ถือบัตรแคร์การ์ด'),
                          textField('เลขบัตรประชาชน', Icons.camera_alt),
                          textField('ชื่อ - นามสกุล', null),
                          textField('เบอร์โทรศัพท์', null),
                          textField('อีเมล', null),
                          button_next('ดำเนินการต่อ', 1),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ยกเลิก',
                            ),
                          )
                        ],
                      )
                    );
                    break;

              //     case 2:
              //       return new Container(
              //         height: 450,
              //         decoration: BoxDecoration(
              //           borderRadius: new BorderRadius.only(
              //             topLeft: const Radius.circular(20),
              //             topRight: const Radius.circular(20)),
              //           image: DecorationImage(
              //             image: AssetImage("images/bgOrange.png"), fit: BoxFit.fill)),
              //         // color: Colors.transparent, //could change this to Color(0xFF737373), //so you don't have to change MaterialApp canvasColor
              //         padding: EdgeInsets.only(top: 20),
              //         child: ListView(
              //           padding: const EdgeInsets.only(left: 10, right: 10),
              //           scrollDirection: Axis.vertical,
              //           children: <Widget>[
              //             textHeading('กรอกรหัส OTP เพื่อยืนยัน'),
              //             button_next('ดำเนินการต่อ', 1),
              //             FlatButton(
              //               onPressed: () {},
              //               child: Text('ยกเลิก',
              //               ),
              //             )
              //           ],
              //         )
              //       );
              //       break;

                  default:
                }
              });
            },
            child: Container(
              margin: const EdgeInsets.all(10),
              width: 300,
              height: 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.ac_unit),
                  Text(nameCard, style: TextStyle(color: Colors.teal[900])),
                ],
              )
            ),
          )
        ),
      );
    }

    Container cardModal(){
      return Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            cardSelect('สแกนบัตรประชาชน', 1),
            cardSelect('กรอกข้อมูลด้วยตัวเอง', 1),
          ],
        )
      );
    }

    Widget button = SizedBox(
      width: 10,
      height: 50,
      child: RaisedButton(
        onPressed: () => {
          showRoundedModalBottomSheet(
              context: context,
              radius: 20, // This is the default
              color: Colors.white, // Also default
              builder: (builder) {
                return new Container(
                  height: 220,
                  color: Colors.transparent, //could change this to Color(0xFF737373), //so you don't have to change MaterialApp canvasColor
                  padding: EdgeInsets.only(top: 20),
                  child: ListView(
                    padding: const EdgeInsets.only(left: 10,right: 10),
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                      textHeading('เลือกวิธีการเพิ่มบัครแคร์การ์ด'),
                      cardModal(),
                    ],
                  )
                );
              })
        },
        child: Text('+ เพิ่มบัตรแคร์การ์ด'),
      ),
    );

    Container myCard(String color, String heading, String subtitle, String imageasset){
      return Container(
              width: 150,
              child: Card(
                  // color: Colors.lightGreen[700],
                  color: new Color(int.parse(color.substring(1, 7), radix: 16) + 0xFF000000),
                  child: InkWell(
                    splashColor: Colors.blue.withAlpha(30),
                    onTap: () {
                      print('Card tapped.');
                    },
                    child: Container(
                      padding: const EdgeInsets.only(left: 10,right: 10,top: 15,bottom: 15),
                      child: Wrap(
                        children: <Widget>[
                          Image.asset(imageasset,width: 35,height: 35),
                          ListTile(
                            title: Text( heading,
                              style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white)),
                            subtitle: Text(subtitle,
                             style: TextStyle(color: Colors.white)),
                          )
                        ],
                      ),
                    ),
                  )
              ),
            );
    }

    Container card(){
      return Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: 180,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              myCard('#45B39D', 'My Policy', 'ข้อมูลกรมธรรม์', 'images/document.png'),
              myCard('#85C1E9', 'My Claim', 'ติดตามสินไหม', 'images/claim.png'),
              myCard('#AED6F1', 'My Hospital', 'รพ.ในเครือข่าย', 'images/hospital.png'),
              myCard('#2E86C1', 'Contact Us', 'ตอบทุกข้อสงสัย', 'images/agenda.png'),
            ],
          ),
        );
    }

    return Scaffold(
      body: ListView(
        padding: const EdgeInsets.all(10),
        scrollDirection: Axis.vertical,
        children: <Widget>[
          card(),
          button
        ],
      ),
    );
  }
}