import 'package:flutter/material.dart';
import 'package:mobile_flutter_research/page/first.dart';
// import 'package:mobile_flutter_research/page/second.dart';
import 'package:mobile_flutter_research/page/third.dart';
import 'package:mobile_flutter_research/page/four.dart';
import 'package:mobile_flutter_research/page/five.dart';
import 'package:mobile_flutter_research/page/todo_list/Screens/todo_list.dart';
import 'package:badges/badges.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:async';
import 'dart:convert';
import 'package:custom_switch/custom_switch.dart';

void main() => runApp(MyApp());

class MenuList {
  final String accountSetting;
  final String manageYourPolicy;
  final String pushNotification;
  final String appLanguage;
  final String termsConditions;
  final String faq;
  final String logOut;

  MenuList (
    this.accountSetting,
    this.manageYourPolicy,
    this.pushNotification,
    this.appLanguage,
    this.termsConditions,
    this.faq,
    this.logOut
  );

  MenuList.fromJson(Map<String, dynamic> json) :
    accountSetting = json['accountSetting'],
    manageYourPolicy = json['manageYourPolicy'],
    pushNotification = json['pushNotification'],
    appLanguage = json['appLanguage'],
    termsConditions = json['termsConditions'],
    faq = json['faq'],
    logOut = json['logOut'];

  Map<String, dynamic> toJson() => {
    'accountSetting': accountSetting,
    'manageYourPolicy': manageYourPolicy,
    'pushNotification': pushNotification,
    'appLanguage': appLanguage,
    'termsConditions': termsConditions,
    'faq': faq,
    'logOut': logOut,
  };
}

Map<String, dynamic> menuMap;
String language = 'en';
bool switchLanguage = true;

class MyApp extends StatelessWidget {
  Future<String> loadJsonData() async{
    print(menuMap);
    if(switchLanguage == true){
      switchLanguage = false;
      language = 'th';
    }else{
      switchLanguage = true;
      language = 'en';
    }
    var jsonText = await rootBundle.loadString('assets/'+language+'.json');
    menuMap = jsonDecode(jsonText);
    print(menuMap);
  }

  @override
  Widget build(BuildContext context) {
    this.loadJsonData();
    return MaterialApp(
      home: new PageDrawer(),
      theme: ThemeData(
        primarySwatch: Colors.orange,
        canvasColor: new Color(int.parse('#ECECEC'.substring(1, 7), radix: 16) + 0xFF000000)
      ),
    );
  }
}

class DrawerItem {
  String title;
  IconData icon;
  IconData trailing;
  DrawerItem(this.title, this.icon, this.trailing);
}

class PageDrawer extends StatefulWidget {
  final drawerItems = [
    new DrawerItem(menuMap['menuList']['accountSetting'], Icons.face, Icons.arrow_forward_ios),
    new DrawerItem(menuMap['menuList']['manageYourPolicy'], Icons.book, Icons.arrow_forward_ios),
    new DrawerItem(menuMap['menuList']['pushNotification'], Icons.notifications_active, null),
    new DrawerItem(menuMap['menuList']['appLanguage'], Icons.language, null),
    new DrawerItem(menuMap['menuList']['termsConditions'], Icons.description, Icons.arrow_forward_ios),
    new DrawerItem(menuMap['menuList']['faq'], Icons.comment, Icons.arrow_forward_ios),
    new DrawerItem(menuMap['menuList']['logOut'], Icons.system_update_alt, null)
  ];

  @override
  State<StatefulWidget> createState() {
    return new PageState();
  }
}

class PageState extends State<PageDrawer> {
  int _selectedDrawerIndex = 0;
  int counter = 0;

  _getDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
        return new FirstPage();
      case 1:
        return new TodoList();
      case 2:
        return new ThirdPage();
      case 3:
        return new FourPage();
      case 4:
        return new FivePage();
      default:
        return new Text("Error");
    }
  }

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop(); // close the drawer
  }

  bool status = true;
  @override
  Widget build(BuildContext context) {
    List<Widget> drawerOptions = [];
    for (var i = 0; i < widget.drawerItems.length; i++) {
      var d = widget.drawerItems[i];
      switch (i) {
        case 2:
          drawerOptions.add(new ListTile(
            leading: new Icon(d.icon),
            title: new Text(d.title),
            trailing: GestureDetector(
              child: Container(
                width: 50,
                height: 25,
                child: CustomSwitch(
                    activeColor: new Color(int.parse('#00bea0'.substring(1, 7), radix: 16) + 0xFF000000),
                    value: status,
                    onChanged: (value) {
                      print("VALUE : $value");
                      setState(() {
                        status = value;
                      });
                    },
                  ),
              )
            ),
            selected: i == _selectedDrawerIndex,
            onTap: () => _onSelectItem(i),
          ));
          break;
        case 3:
          drawerOptions.add(new ListTile(
            leading: new Icon(d.icon),
            title: new Text(d.title),
            trailing: GestureDetector(
              child: Container(
                width: 50,
                height: 25,
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  image: DecorationImage(
                    image: AssetImage('images/'+language+'.png'),
                    fit: BoxFit.fill
                  ),
                )),
                onTap:(){
                  print("you clicked AppLanguage");
                  if(switchLanguage == true){
                    setState(() {
                      switchLanguage = false;
                      language = 'th';
                    });
                  }else{
                    setState(() {
                      switchLanguage = true;
                      language = 'en';
                    });
                  }
                }
            ),
            selected: i == _selectedDrawerIndex,
            onTap: () => _onSelectItem(i),
          ));
          break;
        default:
          drawerOptions.add(new ListTile(
            leading: new Icon(d.icon),
            title: new Text(d.title),
            trailing: Icon(d.trailing),
            selected: i == _selectedDrawerIndex,
            onTap: () => _onSelectItem(i),
          ));
      }
    }

    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    return new Scaffold(
      key: _scaffoldKey,
      endDrawer: new Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text(''),
              decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(
                  image: ExactAssetImage('images/fwd.png'),
                  fit: BoxFit.none,
                )
              ),
            ),
            new Column(children: drawerOptions)
          ],
        ),
      ),
      appBar: new AppBar(
          title: new Text(
            'FWD Healthcare',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          iconTheme: new IconThemeData(color: Colors.yellow[400]),
          actions: <Widget>[
            Badge(
              animationType: BadgeAnimationType.scale,
              padding: const EdgeInsets.all(5),
              position: BadgePosition(right: 5, top: 2),
              badgeColor: Colors.tealAccent[700],
              badgeContent: Text(counter.toString(),style: TextStyle(color: Colors.white)),
              child: IconButton(
                icon: const Icon(Icons.notifications),
                onPressed: () {
                  setState(() {
                    counter = 0;
                  });
                },
              ),
            ),
            IconButton(
              icon: const Icon(Icons.menu),
              onPressed: () => _scaffoldKey.currentState.openEndDrawer(),
            )
          ]),
          // floatingActionButton: FloatingActionButton(onPressed: () {
          //   print("Increment Counter");
          //     setState(() {
          //       counter++;
          //     }
          //   );
          // }, child: Icon(Icons.add),),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
    );
  }
}
